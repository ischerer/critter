(function (root, factory) {
    if (typeof define === 'function' && define.amd) {
        define([], factory);
    } else if (typeof module === 'object' && module.exports) {
        module.exports = factory();
    } else {
        root.Critter = factory();
  }
}(this, function () {

    var Critter = function(items /*, options*/) {
            // options || (options = {});
            this._reset();

            this.initialize.apply(this, arguments);

            this.add(items);
            this.update();
        },
        fn = Critter.prototype,
        toString = Object.prototype.toString,
        hasOwnProperty = Object.prototype.hasOwnProperty;

    fn.initialize = function(){};

    fn._reset = function() {
        this.items = [];
        this.idx = 0;
        this._idCount = 0;
        this._byId = {};
    };

    fn._each = function(array, fun, context) {
        var i = 0,
            length = array.length;
        context = context || this;
        for (; i < length; i++) {
            if (i in array) {
                fun.call(context, array[i], i, array);
            }
        }
        return array;
    };

    fn._extend = function(obj /*, obj2, obj3, ... */) {
        if (!fn.isObject(obj)) return obj;
        var i = 1,
            length = arguments.length,
            base, property;
        for ( ; i < length; i++) {
            base = arguments[i];
            for (property in base) {
                if (hasOwnProperty.call(base, property)) {
                    obj[property] = base[property];
                }
            }
        }
        return obj;
    };

    fn._generateID = function() {
        return ''+parseInt(Date.now()+(this._idCount++),10);
    };

    fn._assignIds = function(arr) {
        this._each(arr,function(obj){
            obj._id = obj._id !== void 0 ? obj._id : this._generateID();
            this._byId[obj._id] = obj;
        });
    };

    fn._clone = function(obj) {
        if (fn.isObject(obj)) return fn._extend({},obj);
        if (fn.isArray(obj)) return obj.slice();
        return obj;
    };

    // isFunction, isString, isNumber, isObject
    fn._each(['Function','String','Number','Object'],function(type) {
        fn['is'+type] = function(obj) {
            return toString.call(obj) === '[object '+type+']';
        };
    });

    fn.isArray = Array.isArray || function(obj) {
        return toString.call(obj) === '[object Array]';
    };
    
    fn.next = function() {
        // Make this possibly, optionally run initialize / unload methods if the object has them?
        // optionally allow for this to return to the beginning of the critter array when on the last index
        var l = this.items.length;
        if (this.idx < l - 1) {
            this.idx++;
            this.update();
            return this.items[this.idx];
        } else {
            // if we're on the last index, then there isn't one following it.
            return undefined;
        }
    };

    fn.current = function() {
        return this.items[this.idx];
    };

    fn.prev = function() {
        // Make this possibly, optionally run initialize / unload methods if the object has them?
        // optionally allow for this to return to the end of the critter array when on the first index
        if (this.idx > 0) {
            this.idx--;
            this.update();
            return this.items[this.idx];
        } else {
            // if we're on the first index, then there isn't one before it.
            return undefined;
        }
    };

    fn.get = function(obj) {
        if (obj == null) return void 0;
        return this._byId[obj] || this._byId[obj._id];
    };

    fn.makeCurrent = function(id) {
        // Make this possibly, optionally run initialize / unload methods if the object has them?
        var obj = this.get(id);
        this.idx = this.items.indexOf(obj);
        return this;
    };

    fn.goToIndex = function(at) {
        // Make this possibly, optionally run initialize / unload methods if the object has them?
        at = at !== void 0 ? at : this.idx;
        this.update();
        return (at < this.items.length || at >= 0) ? this.items[this.idx = at] : undefined;
    };

    fn.set = function(at,val) {
        if (arguments.length === 1) {
            val = at;
            at = this.idx;
        }
        this.items[at] = val !== void 0 ? val : this.items[at];
        this.update();
        return this.items[at];
    };

    fn.update = function() {
        this.first = this.idx === 0;
        this.last = this.idx === this.items.length-1;
        this.length = this.items.length;
    };

    fn.state = function() {
        return {
            idx: this.idx,
            items: this.items,
            first: this.first,
            last: this.last
        };
    };

    fn.where = function(a, context) { // Could port this over to the collections framework
        var i = 0,
            l = this.items.length,
            rv = [],
            parts,left,op,right,match;
        // if 1 argument, check if function. if string, check if it's a comparison, else default to id?
        if (typeof a === 'function') {
            // so 'a' is a compare function of sorts...
            context = context || this;
            for( ; i<l; i++) {
                match = a(context, this.items[i], i, this.items);
                if (match) rv.push(this.items[i]);
            }
        } else if (typeof a === 'string') {
            // loop over the array and return a new critter containing items that match criteria defined in "a" 
            parts = a.split(' ');
            parts[2] = parts.slice(2).join(' '); // if the right hand side contains spaces, it will be rejoined
            for ( ; i<l; i++) {
                left = fn.dig(this.items[i],parts[0]);
                op = parts[1];
                right = parts[2];
                switch (op) {
                    case '==':
                        match = left == right;
                        break;
                    case '!=':
                        match = left != right;
                        break;
                    case '===':
                        match = left === right;
                        break;
                    case '!==':
                        match = left !== right;
                        break;
                    case '>=':
                        match = left >= right;
                        break;
                    case '<=':
                        match = left <= right;
                        break;
                    case '>':
                        match = left > right;
                        break;
                    case '<':
                        match = left < right;
                        break;
                    default:
                        match = false;
                }
                if (match) rv.push(this.items[i]);
            }
        }
        return new Critter(rv);
    };

    fn.each = function(fun, context) {
        this._each(this.items, fun, context);
        return this;
    };

    fn.add = function(val,at){
        var single = !this.isArray(val),
            i = 0,
            length;
        val = single ? (val ? [val] : []) : val;
        length = val.length;

        if (length) {
            this._assignIds(val);
            if (at !== void 0) {
                this.items = this.items.slice(0,at).concat(val).concat(this.items.slice(0));
            } else {
                for( ; i < length; i++) {
                    this.items.push(val[i]);
                }
            }
        }

        this.update();
        return this;
    };

    fn.remove = function(items /*, options*/) {
        var single = !fn.isArray(items),
            i, l, item, idx;
        items = single ? [items] : fn._clone(items);
        // options = options || {};

        for (i = 0, l = items.length; i < l; i++) {
            item = items[i];
            idx = this.items.indexOf(item);
            delete this._byId[item._id];
            this.items.splice(idx,1);

            // if(!options.silent) {
            //     item.trigger('remove',[item]);
            //     this.trigger('remove',[item]);
            // }
        }

        this.update();
        return single ? items[0] : items;
    };

    fn.removeAt = function(at) {
        // if 'at' is undefined, it should default to current index.
        if (at !== void 0) this.items.splice(at,1);
        this.update();
        return this;
    };

    fn.sort = function(val, order) {
        // accepts a string used in dig, or a compare function
        // add ignoring of case-sensitivity?
        // make this so the current critter index remains current after sorting, we don't want to lose track of what we are currently on.
        var compareValues,
            properties;

        if (typeof val === 'string') {
            order = order !== void 0 ? order : 'asc';
            properties = val.split(' ');

            compareValues = function(a, b, props, order) {
                var x = fn.dig(a,props[0]),
                    y = fn.dig(b,props[0]),
                    rv, propsLeft;

                propsLeft = fn._clone(props);
                propsLeft.shift();

                if (x > y) {
                    rv = 1;
                } else if (x < y) {
                    rv = -1;
                } else {
                    if (propsLeft.length) {
                        rv = compareValues(a, b, propsLeft, 'asc');
                    } else {
                        rv = 0;
                    }
                }
                
                if (order !== 'asc') rv = rv * -1;
                
                return rv;
            };

            this.items.sort(function(a, b) {
                return compareValues(a, b, properties, order);
            });
        } else if (typeof val === 'function') {
            this.items.sort(val);
        } else {
            this.items.sort();
        }
        return this;
    };

    fn.dig = function(o,str) { // Could port this over to the collections framework
        // given a value and a string representing a path to a value, check the value for keys and return the value of the last subkey (or undefined if it does not exist)
        // - Ex: dig({value:{key:{subkey:"3"}}}, 'value.key.subkey') ---> "3"
        var a, n;
        if (arguments.length === 1) {
            str = o;
            o = this.current();
        } 
        a = str.replace(/\[(\w+)\]/g, '.$1').replace(/^\./, '').split('.'); // convert indexes to properties, strip a leading dot, split.
        while (a.length) {
            n = a.shift();
            if (n in o) {
                o = o[n];
            } else {
                return;
            }
        }
        return o;
    };

    return Critter;
}));